import unittest
from Coche import Coche
class CocheTestCase(unittest.TestCase):
    #Aqui estoy llamando a unittes para poder ejecutarlo y TestCase es un objeto preterminado de unites
    def test_velocidad(self):
        coche2 = Coche('Carlos Sainz','Rojo','Ferrari','SF-24','5555SAI',155)
        aumento_de_velocidad = 10
        self.assertEqual(coche2.calcula_velocidad(aumento_de_velocidad), 165)
    def test_frenado(self):
        coche2 = Coche('Carlos Sainz','Rojo','Ferrari','SF-24','5555SAI',155)
        decrecimiento_de_velocidad = 20
        self.assertEqual(coche2.calcula_frenado(decrecimiento_de_velocidad), 135)
    def test_distancia(self):
        coche2 = Coche('Carlos Sainz','Rojo','Ferrari','SF-24','5555SAI',155)
        tiempo_transcurrido = 30
        aumento_de_velocidad = 10
        self.assertNotEqual(coche2.calcula_distancia(tiempo_transcurrido, aumento_de_velocidad), 2343)
#Es una forma de iniciar el unitest main

if __name__ == '__main__':
    unittest.main()
