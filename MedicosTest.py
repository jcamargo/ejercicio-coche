import unittest
from herencia_medico import Medico
from herencia_medico import Paciente
class TestPaciente(unittest.TestCase):
    #Aqui estoy llamando a unittes para poder ejecutarlo y TestCase es un objeto preterminado de unites
   def test_ver_historial_clinico2(self):
        persona2 = Paciente('Candela', 'Luna Rey', '20/03/2005','49485592N','anemia, conjuntivitis')
        historial = persona2.ver_historial_clinico()

        self.assertEqual(historial, 'anemia, conjuntivitis')
class TestMedico(unittest.TestCase):
    #Aqui estoy llamando a unittes para poder ejecutarlo y TestCase es un objeto preterminado de unites
   def test_consultar_agenda1(self):
        persona1 = Medico('Eriká', 'Luna Rey', '20/03/2005','49485592N','12 febrero', 'radiología')
        agenda = persona1.consulta_agenda()

        self.assertEqual(agenda, ' Médic@,especialidad: radiología.Cita:12 febrero')

if __name__ == '__main__':
    unittest.main()