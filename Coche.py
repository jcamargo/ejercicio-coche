class Coche:
    def __init__(self, cond, col, marc, mod , matric ,vel = 0):
        self.conductor = cond
        self.color = col
        self.marca = marc
        self.modelo = mod
        self.matricula = matric
        self.velocidad = vel
    def calcula_velocidad(self, crecimiento):
         velocidad_total = self.velocidad + crecimiento 
         return velocidad_total
    def calcula_frenado(self, decremiciento):
         velocidad_total = self.velocidad - decremiciento
         return velocidad_total
    def calcula_distancia (self, tiempoMRU, crecimiento, xo = 0):
        x = xo + int(((self.calcula_velocidad(crecimiento) * 1000)/3600)* tiempoMRU)
        return x
    def __str__(self):
        return ' {nombre} iba con una velocidad de {vel} km/h, aceleró y llegó a los {vel_acel} km/h.Con esta nueva velocidad avanzó en 30 segundos {distancia} metros hasta que frenó llegando a los {vel_del} km/h'.format(nombre = self.conductor, vel=self.velocidad,vel_acel =self.calcula_velocidad(33),distancia= self.calcula_distancia(30,33), vel_del=self.calcula_frenado(14))
coche1 = Coche('Fernado Alonso','Azul','Renault','R25','3333ALO', 233)
coche2 = Coche('Carlos Sainz','Rojo','Ferrari','SF-24','5555SAI',155)
print(coche1)
print(coche2)



