class Persona:
    def __init__(self,nam,ape,nac,doc):
        self._nombre = nam
        self._apellidos = ape
        self._nacimiento = nac
        self._dni = doc
    def __str__(self):
        return '{nombre} {apellidos}, nacid@ el {nacimiento} con DNI: {nacio}.'.format(nombre= self._nombre,apellidos = self._apellidos, nacimiento = self._nacimiento,nacio = self._dni)
    def setNombre(self, nam):
        self.nombre = nam
    def getNombre(self):
        return self.nombre
    def setApellidos(self, ape):
        self.apellidos = ape
    def getApellidos(self):
        return self.apellidos
    def setNacimiento(self, nac):
        self.nacimiento = nac
    def getNacimiento(self):
        return self.nacimiento
    def setdni(self, doc):
        self.dni = doc
    def getdni(self):
        return self.dni
    
    #Al estar los atributos del constructor init en privado, no podemos cambiar los vaores que hay dentro.
    #Con el metodo set podemos cambiar el valor nuevo, y con el metodo get podemos imprimir lo nuevo cambiado
class Medico(Persona):
    def __init__(self,nam,ape,nac,doc,cit,esp):
        super(). __init__(nam,ape,nac,doc)
        self._citas = cit
        self._especialidad = esp
    def consulta_agenda(self):
        return' Médic@,especialidad: {especialidad}.Cita:{dia}'.format(especialidad = self._especialidad, dia = self._citas)

class Paciente(Persona):
    def __init__(self,nam,ape,nac,doc,hist):
        super(). __init__(nam,ape,nac,doc)
        self._historial_clinico = hist
    def ver_historial_clinico(self):
        return  '{historial}'.format( historial = self._historial_clinico)
        




persona1 = Medico('Eriká', 'Luna Rey', '20/03/2005','49485592N','12 febrero', 'radiología')
persona2 = Paciente('Candela', 'Luna Rey', '20/03/2005','49485592N','anemia, conjuntivitis')
print(persona1.consulta_agenda())
print(persona2.ver_historial_clinico())
#A LA HORA DE HACER PRUEBAS PONER EN LO QUE NO SON LOS TEST :
#if __name__ == '__main__':
    #codido que se imprime